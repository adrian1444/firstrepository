import java.util.Arrays;

public class Calculator {

    public float add(float... numbers){
        float sum=0;
        for(float n:numbers)
            sum+=n;
        return sum;
    }

    public float multiply(float... numbers) {
        return Arrays.stream(numbers).reduce(1, (a, b) -> a * b);
    }
    public float sub(float number1, float number2){
        return number1-number2;
    }
}
